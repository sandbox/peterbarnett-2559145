<?php

/**
 * @file
 * Main module file for User Message.
 */

/**
 * Implements hook_mail_alter().
 */
function user_message_mail_alter(&$message) {

  if ($message['module'] == 'user') {

    $account = $message['params']['account'];

    // Map the mail key to a message bundle
    $message_bundle = 'user_' . $message['key'];

    // Add our restricted user token callback
    $message_init = array(
      'data' => array(
        'token options' => array(
          // @see user_message_restricted_tokens()
          'callback' => 'user_message_restricted_tokens',
        ),
      ),
    );

    $message_entity = message_create($message_bundle, $message_init, $account);

    $options = array(
      'rendered fields' => array(),
      // For security reasons, prevent saving
      'save on success' => FALSE,
      'save on fail' => FALSE,
    );

    message_notify_send_message($message_entity, $options);

    // Prevent sending of original message
    // @todo: conditional?
    $message['send'] = FALSE;

    return;
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function user_message_form_message_type_form_alter(&$form, &$form_state) {
  if ($form_state['message_type']->category == 'user_message') {
    // Show restricted user tokens in this context
    $form['token_tree']['#show_restricted'] = TRUE;
  }
}


/** 
 * Implements hook_form_FORM_ID_alter().
 */
function user_message_form_user_admin_settings_alter(&$form, &$form_state, $form_id) {
  
  // $form['email']['#access'] = FALSE;
  // @todo: direct user to Message Type admin
}


/**
 * Token callback to add unsafe tokens for user notifications.
 * Follows the pattern used in core by the User module.
 * 
 * @see user_mail_tokens()
 */
function user_message_restricted_tokens(&$replacements, $data, $options) {
  if (isset($data['message']) && $account = user_load($data['message']->uid)) {
    $replacements['[message:user:one-time-login-url]'] = user_pass_reset_url($account);
    $replacements['[message:user:cancel-url]'] = user_cancel_url($account);
  }
}


/**
 * Implements hook_default_message_type_category().
 */
function user_message_default_message_type_category() {
  $items = array();

  $values = array(
    'category' => 'user_message',
    'description' => 'Replaces core user mail notifications',
    'language' => '',
  );

  // A message type category to identify our core user replacements
  $items['user_message'] = entity_create('message_type_category', $values);

  return $items;
}


/**
 * Helper to fetch default content for user messages
 */
function _user_message_mail_text($key) {

  // Invoke the User module helper to get current content
  $text = _user_mail_text($key, NULL, array(), FALSE);

  // Replace 'user' tokens with chained 'message:user' tokens
  return preg_replace('/\[user:([^\[\]]*)\]/x', '[message:user:$1]', $text);
}


/**
 * Implements hook_default_message_type().
 */
function user_message_default_message_type() {

  // User module mail keys
  $mail_keys = array(
    'register_no_approval_required'   => t('Welcome (no approval required)'),
    'register_admin_created'          => t('Welcome (new user created by administrator)'),
    'register_pending_approval'       => t('Welcome (awaiting approval)'),
    'register_pending_approval_admin' => t('Welcome (awaiting approval - admin)'),
    'password_reset'                  => t('Password recovery'),
    'status_activated'                => t('Account activation'),
    'status_blocked'                  => t('Account blocked'),
    'cancel_confirm'                  => t('Account cancellation confirmation'),
    'status_canceled'                 => t('Account canceled'),
  );

  // Define a new message type for each
  foreach ($mail_keys as $key => $description) {

    $machine_name = 'user_' . $key;

    $values = array(
      'name' => $machine_name,
      'description' => $description,
      'category' => 'user_message',
      'data' => array(
        'token options' => array(
          'clear' => TRUE,
        ),
        'purge' => array(
          'override' => FALSE,
          'enabled' => FALSE,
          'quota' => '',
          'days' => '',
        ),
      ),
    );

    $items[$machine_name] = entity_create('message_type', $values);

    // Extract message content from core user config
    $items[$machine_name]->message_text[LANGUAGE_NONE] = array(
      array(
        'value' => _user_message_mail_text($key . '_subject'),
        'format' => 'user_message',
      ),
      array(
        'value' => _user_message_mail_text($key . '_body'),
        'format' => 'user_message',
      ),
    );
  }

  return $items;
}

